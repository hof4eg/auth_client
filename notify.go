package auth_client

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/hof4eg/auth_client/domain"
)

func (cl *Client) Notify(ctx context.Context, userID string, notify domain.Notify) error {
	path := "/v1/server/user/" + userID + "/notify"

	bts, err := json.Marshal(notify)
	if err != nil {
		return fmt.Errorf("notify: %w", err)
	}

	rq, err := http.NewRequestWithContext(ctx, http.MethodPost, cl.apiURL.String()+path, bytes.NewReader(bts))
	if err != nil {
		return fmt.Errorf("notify: %w", err)
	}

	_, status, err := cl.processRequest(rq)
	if err != nil {
		return fmt.Errorf("notify: %w", err)
	}

	if status != http.StatusOK {
		return ErrUnauthorized
	}

	return nil
}
