package auth_client

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/hof4eg/auth_client/domain"
)

func (cl *Client) GetAccount(ctx context.Context, accountID string) (domain.AccountRender, error) {
	path := "/v1/server/account/" + accountID
	rnd := domain.AccountRender{}

	rq, err := http.NewRequestWithContext(ctx, http.MethodGet, cl.apiURL.String()+path, nil)
	if err != nil {
		return rnd, fmt.Errorf("get user: %w", err)
	}

	btx, status, err := cl.processRequest(rq)
	if err != nil {
		return rnd, fmt.Errorf("get user: %w", err)
	}

	if status != http.StatusOK {
		return rnd, ErrUnauthorized
	}

	if err := json.Unmarshal(btx, &rnd); err != nil {
		return rnd, fmt.Errorf("check request: %w", err)
	}

	return rnd, nil
}
