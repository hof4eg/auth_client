package auth_client

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"gitlab.com/hof4eg/auth_client/domain"
)

type Client struct {
	httpClient  http.Client
	apiURL      url.URL
	logger      io.Writer
	serverToken string
}

type Option func(cl *Client) error

func WithTransport(transport *http.Transport) func(cl *Client) error {
	return func(cl *Client) error {
		cl.httpClient.Transport = transport

		return nil
	}
}

func WithLogger(logger io.Writer) func(cl *Client) error {
	return func(cl *Client) error {
		cl.logger = logger

		return nil
	}
}

func New(apiURL url.URL, timeout time.Duration, serverToken string, options ...Option) (*Client, error) {
	cl := &Client{
		httpClient: http.Client{
			Transport: http.DefaultTransport,
			Timeout:   timeout,
		},
		apiURL:      apiURL,
		serverToken: serverToken,
	}

	for i := range options {
		if err := options[i](cl); err != nil {
			return nil, err
		}
	}

	return cl, nil
}

func (cl *Client) processRequest(rq *http.Request) (body []byte, status int, err error) {
	rq.Header.Set(domain.ServerTokenHeaderName, cl.serverToken)

	rsp, err := cl.httpClient.Do(rq)
	if err != nil {
		return nil, 0, fmt.Errorf("check request: %w", err)
	}

	defer rsp.Body.Close()

	bt, err := io.ReadAll(rsp.Body)
	if err != nil {
		return bt, http.StatusInternalServerError, fmt.Errorf("process: %w", err)
	}

	if cl.logger != nil {
		_, _ = cl.logger.Write(bt)
		_, _ = cl.logger.Write([]byte("status: "))
		_, _ = cl.logger.Write([]byte(strconv.Itoa(status)))
	}

	return bt, rsp.StatusCode, nil
}
