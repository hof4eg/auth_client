package domain

import (
	"net/http"

	"github.com/cadyrov/goerr"
)

type SearchForm struct {
	Query       string  `json:"query"`
	IDs         []int64 `json:"ids"`
	ExcludedIDs []int64 `json:"excludedIds"`
	Limit       int     `json:"limit"`
	Page        int     `json:"page"`
}

func BadErr(e error) goerr.IError {
	return err(e).HTTP(http.StatusBadRequest)
}

func AuthErr(e error) goerr.IError {
	return err(e).HTTP(http.StatusUnauthorized)
}

func IntErr(e error) goerr.IError {
	return err(e).HTTP(http.StatusInternalServerError)
}

func ConfErr(e error) goerr.IError {
	return err(e).HTTP(http.StatusConflict)
}

func ForbidErr(e error) goerr.IError {
	return err(e).HTTP(http.StatusForbidden)
}

func NotFoundErr(e error) goerr.IError {
	return err(e).HTTP(http.StatusNotFound)
}

func err(err error) goerr.IError {
	return goerr.New(err.Error())
}
