package domain

import (
	"errors"
	"fmt"
)

type NotifyType string

type Notify struct {
	NotifyType NotifyType `json:"notify_type" description:"Тип уведомления"`
	EventType  string     `json:"event_type" description:"Тип события"`
	Msg        string     `json:"msg" description:"Тело сообщения"`
	EventID    string     `json:"event_id" description:"Идентификатор события" `
}

const (
	UserStateNew    = 10
	UserStateActive = 20
	UserStateBlock  = 30

	RoleRbacManage = "rbacmanage"
	RoleAdmin      = "admin"
)

func Roles() map[string]struct{} {
	exists := make(map[string]struct{})

	exists[RoleRbacManage] = struct{}{}
	exists[RoleAdmin] = struct{}{}

	return exists
}

var ErrRoleNotExists = errors.New("role not exists")

func IsRoles(roles ...string) (e error) {
	exists := Roles()

	for i := range roles {
		if _, ok := exists[roles[i]]; ok {
			continue
		}

		return fmt.Errorf("%s: %w", roles[i], ErrRoleNotExists)
	}

	return nil
}

// User domain.
type User struct {
	ID                 int      `json:"id"`
	AuthKey            string   `json:"authKey"`
	PasswordHash       string   `json:"passwordHash"`
	PasswordResetToken *string  `json:"passwordResetToken"`
	Email              string   `json:"email"`
	AccountID          int      `json:"accountId"`
	Status             int      `json:"status"`
	CreatedAt          int      `json:"createdAt"`
	UpdatedAt          int      `json:"updatedAt"`
	Roles              []string `json:"roles"`
	UserForm
}

type Users []User

type UserForm struct {
	Username   string  `json:"username"`
	Surname    *string `json:"surname"`
	Middlename *string `json:"middlename"`
	Phone      *string `json:"phone"`
	Name       *string `json:"name"`
	Position   *string `json:"position"`
}

type UserSearchForm struct {
	Email     string `json:"email"`
	AccountID int
	SearchForm
}

type UserRender struct {
	ID         int            `json:"id"`
	Email      string         `json:"email"`
	AccountID  int            `json:"accountId"`
	Status     int            `json:"status"`
	CreatedAt  int            `json:"createdAt"`
	UpdatedAt  int            `json:"updatedAt"`
	Roles      []string       `json:"roles"`
	Username   string         `json:"username"`
	Surname    *string        `json:"surname"`
	Middlename *string        `json:"middlename"`
	Phone      *string        `json:"phone"`
	Name       *string        `json:"name"`
	Position   *string        `json:"position"`
	Account    *AccountRender `json:"account,omitempty"`
}

func (m *UserRender) HasOne(roles ...string) bool {
	exists := make(map[string]struct{})
	for i := range m.Roles {
		exists[m.Roles[i]] = struct{}{}
	}

	for _, n := range roles {
		if _, ok := exists[n]; ok {
			return true
		}
	}

	return false
}
