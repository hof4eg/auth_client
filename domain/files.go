package domain

import "time"

type FileCreateForm struct {
	Path string        `json:"path" description:"Относительный путь от бакета s3"`
	TTL  time.Duration `json:"ttl" description:"длительность жизни кредов запроса"`
}

type FileCreateDataResponse struct {
	FileCreateResponse FileCreateResponse `json:"data"`
}

type FileCopyForm struct {
	From string `json:"from" description:"Относительный путь от бакета s3 источника"`
	To   string `json:"to" description:"Относительный путь от бакета s3 назначения"`
}

type FileCreateResponse struct {
	Data string `json:"data" description:"url"`
	URL  string `json:"url"  description:"абсолютный путь в файлу на хранилище"`
	Path string `json:"path"  description:"относительный путь в файлу на хранилище"`
}

type FileDropForm struct {
	Path string `json:"path" description:"Относительный путь от бакета s3"`
}

type FileListDataResponse struct {
	FileListResponse FileListResponse `json:"data"`
}

type FileListResponse struct {
	FileInfos FileInfos `json:"list"`
	MaxKeys   *int64    `json:"max_keys"`
}

type FileInfo struct {
	ETag string `json:"s3_tag"`
	Key  string `json:"key"`
	Name string `json:"name"`
	Size int64  `json:"size"`
}

type FileInfos []FileInfo
