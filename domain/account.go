package domain

import (
	"time"
)

type AccountType int

const (
	TypeVendor      AccountType = 10
	TypeDistributor AccountType = 20
	TypePartner     AccountType = 30
)

type AccountRender struct {
	ID            int         `json:"id"`
	CreateAt      time.Time   `json:"createAt"`                // Дата создания
	DeletedAt     *time.Time  `json:"deletedAt"`               // Дата удаления
	Type          AccountType `json:"type"`                    // Тип аккаунта- вендор, дистрибьютер, конечник
	DistributorID *int        `distributor_id:"distributorId"` // ИД дистрибьютера
	Email         *string     `json:"email"`                   // email организации
	Name          string      `json:"name"`                    // Название акаунта
	Inn           *string     `json:"inn"`                     // инн организации
	Kpp           *string     `json:"kpp"`                     // кпп организации
	Address       *string     `json:"address"`                 // адрес организации
	Phone         *string     `json:"phone"`                   // телефон организации
	Contact       *string     `json:"contact"`                 // контактное лицо организации
}

type AccountRenders []AccountRender
