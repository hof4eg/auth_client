package auth_client

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/hof4eg/auth_client/domain"
)

var ErrUnauthorized = errors.New("client unauthorized")

func (cl *Client) Check(ctx context.Context, scope, token string) (domain.UserRender, error) {
	path := "/v1/" + scope + "/check"
	rnd := struct {
		Data domain.UserRender `json:"data"`
	}{}

	rq, err := http.NewRequestWithContext(ctx, http.MethodGet, cl.apiURL.String()+path, nil)
	if err != nil {
		return rnd.Data, fmt.Errorf("check request: %w", err)
	}

	rq.Header.Add("Authorization", "Bearer "+token)

	btx, status, err := cl.processRequest(rq)
	if err != nil {
		return rnd.Data, fmt.Errorf("check request: %w", err)
	}

	if status != http.StatusOK {
		return rnd.Data, ErrUnauthorized
	}

	if err := json.Unmarshal(btx, &rnd); err != nil {
		return rnd.Data, fmt.Errorf("check request: %w", err)
	}

	return rnd.Data, nil
}
