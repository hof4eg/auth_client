package auth_client

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/hof4eg/auth_client/domain"
)

func (cl *Client) GetCredentialsForUpload(ctx context.Context, path string,
	ttl time.Duration) (domain.FileCreateResponse, error) {
	rnd := domain.FileCreateDataResponse{}

	pathURL := "/v1/server/files"
	frm := domain.FileCreateForm{Path: path, TTL: ttl}

	bts, err := json.Marshal(frm)
	if err != nil {
		return rnd.FileCreateResponse, fmt.Errorf("credential for upload: %w", err)
	}

	rq, err := http.NewRequestWithContext(ctx, http.MethodPost, cl.apiURL.String()+pathURL, bytes.NewReader(bts))
	if err != nil {
		return rnd.FileCreateResponse, fmt.Errorf("credential for upload: %w", err)
	}

	btx, status, err := cl.processRequest(rq)
	if err != nil {
		return rnd.FileCreateResponse, fmt.Errorf("credential for upload: %w", err)
	}

	if status != http.StatusOK {
		return rnd.FileCreateResponse, ErrUnauthorized
	}

	if err := json.Unmarshal(btx, &rnd); err != nil {
		return rnd.FileCreateResponse, fmt.Errorf("check request: %w", err)
	}

	return rnd.FileCreateResponse, nil
}

func (cl *Client) DropFile(ctx context.Context, path string) error {
	pathURL := "/v1/server/files/drop"
	frm := domain.FileDropForm{Path: path}

	bts, err := json.Marshal(frm)
	if err != nil {
		return fmt.Errorf("credential for drop: %w", err)
	}

	rq, err := http.NewRequestWithContext(ctx, http.MethodPost, cl.apiURL.String()+pathURL, bytes.NewReader(bts))
	if err != nil {
		return fmt.Errorf("credential for drop: %w", err)
	}

	_, status, err := cl.processRequest(rq)
	if err != nil {
		return fmt.Errorf("credential for drop: %w", err)
	}

	if status != http.StatusOK {
		return ErrUnauthorized
	}

	return nil
}

func (cl *Client) FileList(ctx context.Context, path string) (domain.FileListResponse, error) {
	pathURL := "/v1/server/files"

	dst := domain.FileListDataResponse{}
	rq, err := http.NewRequestWithContext(ctx, http.MethodGet, cl.apiURL.String()+pathURL+"?path="+path, nil)
	if err != nil {
		return dst.FileListResponse, fmt.Errorf("credential for list: %w", err)
	}

	btx, status, err := cl.processRequest(rq)
	if err != nil {
		return dst.FileListResponse, fmt.Errorf("credential for upload: %w", err)
	}

	if status != http.StatusOK {
		return dst.FileListResponse, ErrUnauthorized
	}

	if err := json.Unmarshal(btx, &dst); err != nil {
		return dst.FileListResponse, fmt.Errorf("check request: %w", err)
	}

	return dst.FileListResponse, nil
}

func (cl *Client) CopyFile(ctx context.Context, from, to string) error {
	pathURL := "/v1/server/files/copy"
	frm := domain.FileCopyForm{
		From: from,
		To:   to,
	}

	bts, err := json.Marshal(frm)
	if err != nil {
		return fmt.Errorf("credential for drop: %w", err)
	}

	rq, err := http.NewRequestWithContext(ctx, http.MethodPost, cl.apiURL.String()+pathURL, bytes.NewReader(bts))
	if err != nil {
		return fmt.Errorf("credential for drop: %w", err)
	}

	_, status, err := cl.processRequest(rq)
	if err != nil {
		return fmt.Errorf("credential for drop: %w", err)
	}

	if status != http.StatusOK {
		return ErrUnauthorized
	}

	return nil
}
