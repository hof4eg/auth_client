package auth_client

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/hof4eg/auth_client/domain"
)

func (cl *Client) GetUser(ctx context.Context, userID string) (domain.UserRender, error) {
	path := "/v1/server/user/" + userID
	rnd := domain.UserRender{}

	rq, err := http.NewRequestWithContext(ctx, http.MethodGet, cl.apiURL.String()+path, nil)
	if err != nil {
		return rnd, fmt.Errorf("get user: %w", err)
	}

	btx, status, err := cl.processRequest(rq)
	if err != nil {
		return rnd, fmt.Errorf("get user: %w", err)
	}

	if status != http.StatusOK {
		return rnd, ErrUnauthorized
	}

	if err := json.Unmarshal(btx, &rnd); err != nil {
		return rnd, fmt.Errorf("check request: %w", err)
	}

	return rnd, nil
}
